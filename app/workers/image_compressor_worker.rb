class ImageCompressorWorker
  include Sidekiq::Worker
  
  def perform(image_id)
    image = Image.find(image_id)
    Tinify.key = Rails.application.credentials.tinifier_key
    source = Tinify.from_url(image.file.service_url)
    source.to_file(image.compressed_file_path)
  end
end