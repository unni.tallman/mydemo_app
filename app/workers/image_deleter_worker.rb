class ImageDeleterWorker
  include Sidekiq::Worker
  
  def perform(image_id)
    image = Image.find(image_id)
    image.destroy
  end
end