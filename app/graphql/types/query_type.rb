module Types
  class QueryType < Types::BaseObject
    field :all_quotes, [QuoteType], null: false
    field :random_quote, [QuoteType], null: false

    def all_quotes
      Quote.all
    end

    def random_quote
      Quote.order(Arel.sql('RANDOM()')).limit(1)
    end
  end
end
