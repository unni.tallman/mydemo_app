module Types
  class QuoteType < BaseObject
    field :id, ID, null: false
    field :text, String, null: false
    field :author, String, null: false
  end
end