class SitesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :check_if_logged_in, except: [:index, :login, :signup]
  before_action :set_heading

  def index
    if logged_in?
      @sites = current_user.sites
    else
      @sites = UptimeMonitor::Site.none
    end
  end

  def new
    @site = UptimeMonitor::Site.new
  end

  def edit
    @site = UptimeMonitor::Site.find(params[:id])
  end

  def update
    @site = UptimeMonitor::Site.find(params[:id])

    if @site.update_attributes(site_params)
      redirect_to sites_path
    else
      render 'edit'
    end
  end

  def create
    @site = current_user.sites.new(site_params)
  
    if @site.save
      redirect_to sites_path
    else
      render 'new'
    end
  end

  def destroy
    @site = UptimeMonitor::Site.find(params[:id])
    @site.destroy

    redirect_to sites_path
  end

  def login
    if request.get?
      @user = UptimeMonitor::User.new
    else
      do_login
    end
  end

  def signup
    if request.get?
      @user = UptimeMonitor::User.new
    else
      do_signup
    end
  end

  def logout
    cookies[:auth_token] = ""
    redirect_to login_sites_path
  end

  private

  def do_login
    @user =UptimeMonitor::User.where(email: params[:uptime_monitor_user][:email]).first

    if @user && @user.valid_password?(params[:uptime_monitor_user][:password])
      set_current_user(@user)
      redirect_to sites_path
    else
      flash[:error_display] = "message-visible"
      flash[:message]       = "Email/Password does not match" 

      redirect_to login_sites_path
    end
  end

  def do_signup
    @user = UptimeMonitor::User.new(user_params)
  
    if @user.save
      flash[:success_display] = "message-visible"
      flash[:message] = "Registration completed. Please login to continue"
      redirect_to login_sites_path
    else
      flash[:error_display] = "message-visible"
      flash[:message] = "Signup Failed" 

      render 'signup'
    end
  end

  def site_params
    params.require(:uptime_monitor_site).permit(:name, :url)
  end

  def user_params
    params.require(:uptime_monitor_user).permit(:name, :email, :password)
  end

  private

  def check_if_logged_in
    unless logged_in?
      flash[:message] = "Please login or signup to continue"
      redirect_to login_sites_path
    end
  end

  def logged_in?
    current_user.present?
  end

  def current_user
    @_current_user ||= UptimeMonitor::User.where(auth_token: cookies[:auth_token]).first 
  end

  helper_method :current_user, :logged_in?

  def set_current_user(user)
    cookies[:auth_token] = user.auth_token
    @_current_user = user
  end

  def set_heading
    p cookies[:auth_token]
    @heading = "Uptime Monitor"
    flash[:success_display] = "message-hidden" unless flash[:success_display].present?
    flash[:error_display]   = "message-hidden" unless flash[:error_display].present?
  end
end
