class PagesController < ApplicationController
  def home
    @heading = "Examples"
  end

  def source_code
    @heading = "Source code for the demo app"
  end

  def who_am_i
    @heading = "Unnikrishnan KP"
  end
end
