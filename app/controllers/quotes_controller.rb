class QuotesController < ApplicationController
  def index
    @heading = "Inspirational Quotes"
    @quote = Quote.order(Arel.sql('RANDOM()')).last
  end

  def show
    @heading = "Inspirational Quotes"
    @quote = Quote.find(params[:id])
  end
end
