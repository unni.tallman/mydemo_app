class ImagesController < ApplicationController
  before_action :set_heading, only: [:index, :show]
  protect_from_forgery with: :null_session

  def create
    image = Image.create(image_params)
    ImageCompressorWorker.perform_async(image.id)
    ImageDeleterWorker.perform_at(30.minutes.from_now, image.id)

    redirect_to image_path(image)
  end

  def show
    @image = Image.find(params[:id])
  end

  def index
  end

  private

  def image_params
    params.require(:image).permit(:file)
  end

  def set_heading
    @heading = "Image Optimizer"
  end
end
