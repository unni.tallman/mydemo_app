class Api::ImagesController < ApplicationController
  def show
    image = Image.find(params[:id])

    render json: {
      original: url_for(image.file), 
      compressed: (image.compressed? ? "/#{image.compressed_file_url_path}" :nil)
    }
  end
end