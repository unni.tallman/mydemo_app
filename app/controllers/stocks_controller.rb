class StocksController < ApplicationController
  def show
    render json: Stock.new(params[:id]).get_quote
  end

  def index
    @heading = "Stocks Watchlist"
  end
end
