class Image < ApplicationRecord
  has_one_attached :file

  def compressed_file_path
    Rails.root.join("public/compressed_images/#{self.file.filename}")
  end

  def compressed_file_url_path
    compressed? ? "compressed_images/#{self.file.filename}" : nil
  end

  def compressed?
    File.exists?(compressed_file_path)
  end

  def destroy
    super
    `rm #{compressed_file_path}`
  end
end