module UptimeMonitor
  class Site < ApplicationRecord
    validates_presence_of :name, :url

    belongs_to :user, class_name: 'UptimeMonitor::User', foreign_key: 'user_id'

    def status
      up? ? 'UP' : 'DOWN'
    end
  end
end
