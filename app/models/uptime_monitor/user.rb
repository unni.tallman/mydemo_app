module UptimeMonitor
  class User < ActiveRecord::Base
    before_save :generate_auth_token

    attr_accessor :password

    self.table_name = 'uptime_monitor_users'

    validates_presence_of :name, :email, :encrypted_password

    has_many :sites, class_name: 'UptimeMonitor::Site', foreign_key: 'user_id'

    def password=(my_password)
      self.encrypted_password = Digest::SHA2.new(256).hexdigest(my_password)
    end

    def valid_password?(my_password)
      self.encrypted_password == Digest::SHA2.new(256).hexdigest(my_password)
    end

    private

    def generate_auth_token
      self.auth_token = Digest::SHA2.new(256).hexdigest(Time.now.to_s)
    end
  end
end