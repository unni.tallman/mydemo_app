class Stock
  attr_reader :symbol

  def initialize(symbol)
    @symbol = symbol
  end

  def get_quote
    begin
      quote = IEX::Resources::Quote.get(symbol)
    rescue IEX::Errors::SymbolNotFoundError
      return {}
    end

    {
      company_name: quote.company_name,
      price: quote.delayed_price,
      low: quote.low,
      high: quote.high,
      symbol: quote.symbol
    }
  end
end