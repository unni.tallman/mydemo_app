import React from 'react';
import axios from 'axios';

class StockRow extends React.Component {
  constructor(props){
    super(props)
    this.state = {quote: {symbol: '', price: '', high: '', low: '', company_name: ''}}
  }

  componentWillMount() {
     axios.get(`/stocks/${this.props.symbol}`)
      .then(res => {
        if(res.data.symbol){
          this.setState({quote: res.data});  
        }
      }).catch(error => {
       console.log(error.response)
     });
  }

  render() {
    return(
      <tr>
        <td>{this.state.quote.symbol}</td>
        <td>{this.state.quote.company_name}</td>
        <td>{this.state.quote.price}</td>
        <td>{this.state.quote.high}</td>
        <td>{this.state.quote.low}</td>
        <td>
          <button onClick={this.props.deleteHandler} value={this.state.quote.symbol} className="btn btn-primary">delete</button>  
        </td>
      </tr>
    );
  }
}

export default StockRow;