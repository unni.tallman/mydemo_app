import React from 'react';
import StockRow from './StockRow';
import './style.css'

class StocksTable extends React.Component {
  constructor(props){
    super(props)

    var stocks = localStorage.getItem('stocks');
    stocks = JSON.parse(stocks); 

    if(!stocks){
      stocks = ['AAPL']
    }

    this.state = {stocks: stocks};


    this.handleAdd = this.handleAdd.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleAdd(e) {
    e.preventDefault();
    this.new_symbol = document.getElementById("stock_name").value

    var stocks = this.state.stocks;
    stocks.unshift(this.new_symbol);
    
    this.setState((prevState, props) => ({
      stocks: stocks
    }));

    localStorage.setItem('stocks', JSON.stringify(stocks));

    document.getElementById("stock_name").value = "";
  }

  handleDelete(e) {
    e.preventDefault();
    var stocks = this.state.stocks.filter(function(stock) { return stock != e.target.value });
    this.setState({ stocks: stocks });
    localStorage.setItem('stocks', JSON.stringify(stocks));
  }

  render() {
    return(<div className="stocks-container">
      <form className="stock-search-form">
        <div className="form-group">
          <div className="row">
            <input className="form-control col-lg-8" autoComplete="off" type="text" id="stock_name" />
            &nbsp;
            <button className="btn btn-info col-lg-2" onClick={this.handleAdd}>Add</button>
            
            <small id="passwordHelpBlock" class="form-text text-muted">
              Add stocks using their Symbols. You can get symbols for your favourite stocks from <a target="_blank" href="https://finance.yahoo.com/">Yahoo Finance.</a>
            </small>
            
            
          </div>
        </div>
      </form>


      <table className="table"> 
        <thead>
          <th>Symbol</th>
          <th>Company Name</th>
          <th>Price</th>
          <th>High</th>
          <th>Low</th>
        </thead>
        <tbody>
        {this.state.stocks.map(item => 
          <StockRow key={item} symbol={item} deleteHandler={this.handleDelete} />
        )}
        </tbody>
      </table>

      
      </div>
    );
  }
}

export default StocksTable;