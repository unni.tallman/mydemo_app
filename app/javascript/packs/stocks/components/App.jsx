import React from 'react'
import StocksTable from './StocksTable'

class App extends React.Component {
  render(){
    return(
      <div>
        <StocksTable />
      </div>
    );
  }
}

export default App;