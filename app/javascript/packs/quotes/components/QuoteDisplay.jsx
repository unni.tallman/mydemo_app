import React from 'react';
//import axios from 'axios';

import ApolloClient from "apollo-boost";
import gql from "graphql-tag";

class QuoteDisplay extends React.Component {
  constructor(props){
    super(props)
    this.state = {quote: this.getIntialQuote()}
    this.handleClick = this.handleClick.bind(this);
  }

  getIntialQuote() {
    var dataElement = document.getElementById('my_quote');
    var data = JSON.parse(dataElement.getAttribute('data-quote'));
    return data;
  }


  handleClick() {
    const client = new ApolloClient({uri: "/graphql"});

    client.query({
      query: gql`
        {
          randomQuote {
            text
            author
            id
          }
        }
      `
    }).then(result => {
      this.setState({quote: result.data.randomQuote[0]})
    }) 


    // axios.get(`/quotes/get`)
    //   .then(res => {
    //     console.log(res);
    //     this.setState({quote: res.data});
    //   }).catch(error => {
    //    console.log(error.response)
    //  });
  }

  copyToClipBoard(){
    document.getElementById("my_url").select();
    document.execCommand("Copy");
    alert("Copied to clipboard");
  }

  getUrl(id){
    var url = document.location.href;
    var arr = url.split("/");
    if(arr[arr.length - 1] == "quotes"){
      arr[arr.length] = id;
    }else{
      arr[arr.length - 1] = id;  
    }
    
    url = arr.join("/");
    return url;
  }

  render() {
    var url = this.getUrl(this.state.quote.id);
    history.pushState(null, null, url);
    
    return(
      <div>
        <div className="card card-body bg-light">
        <p>{this.state.quote.text}</p>
        <h5>{this.state.quote.author}</h5>
        </div>
        <hr/>
        <div>
          <input class="col-lg-12" type="text" id="my_url" value={url}></input>
        </div>
        <hr/>
        <button className="btn btn-primary col-lg-2" onClick={this.copyToClipBoard} >Copy Link</button>
        <button className="btn btn-primary col-lg-2 float-right" onClick={this.handleClick} >Next Quote</button>

      </div>
      );
  }
}

export default QuoteDisplay;