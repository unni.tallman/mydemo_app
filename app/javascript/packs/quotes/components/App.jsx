import React from 'react'
import QuoteDisplay from './QuoteDisplay'

class App extends React.Component {
  render(){
    return(
      <div>
        <QuoteDisplay />
      </div>
    );
  }
}

export default App;