import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'

// import ApolloClient from "apollo-boost";
// import gql from "graphql-tag";

const quotes = document.querySelector('#quotes')
ReactDOM.render(<App />, quotes)

// const client = new ApolloClient({uri: "/graphql"});

//     client.query({
//       query: gql`
//         {
//           randomQuote {
//             text
//             author
//           }
//         }
//       `
//     }).then(result => 
//       console.log(result.data.randomQuote[0])
//     ); 