var image_id  = $('#image_id').val();
var image_tag = $('#image_container');
var image_download = $('#image_download');
var image_loader = $('#image_loader');
var compressed_image_components = $('.compressed_image_components');

if(image_tag.attr('src') == "/"){
  image_loader.show();

  var timer = setInterval(function(){
    $.get("/api/images/" + image_id, function(data, status){
      if(data.compressed){
        image_tag.attr('src', data.compressed);
        image_download.attr('href', data.compressed);
        image_loader.hide();
        compressed_image_components.show();
        clearInterval(timer);
      }
    });
  }, 3000);
}else{
  compressed_image_components.show();
}

