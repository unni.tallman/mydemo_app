namespace :deploy do
  desc "Restart Puma"
  task :restart do
    on roles(:app) do
      execute "sudo systemctl restart mydemo_app.service"
    end
  end

  desc "Restart Sidekiq"
  task :restart_sidekiq do
    on roles(:app) do
      execute "sudo systemctl restart mydemo_app_sidekiq.service"
    end
  end
end

after "deploy:finishing", "deploy:restart"
after "deploy:finishing", "deploy:restart_sidekiq"