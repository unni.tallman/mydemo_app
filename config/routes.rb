Rails.application.routes.draw do
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end
  post "/graphql", to: "graphql#execute"

  root to: "pages#home"
  get 'source_code', to: "pages#source_code"
  get 'who-am-i',    to: "pages#who_am_i"
  
  namespace :api, defaults: { format: :json } do
    resources :quotes, only: [ :show ]
    resources :images, only: [ :show ]
  end

  resources :stocks, only: [:show, :index]
  resources :quotes, only: [:index, :show]
  resources :memes,  only: [:index]
  resources :images, only: [:create, :index, :show]
  resources :sites do 
    collection do 
      get :login
      post :login
      get :signup
      post :signup
      get :logout
    end
  end
end