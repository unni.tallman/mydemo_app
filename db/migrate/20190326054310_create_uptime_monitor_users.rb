class CreateUptimeMonitorUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :uptime_monitor_users do |t|
      t.string :name
      t.string :email
      t.string :encrypted_password
      t.string :auth_token
      t.timestamps
    end
  end
end
