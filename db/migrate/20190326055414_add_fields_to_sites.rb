class AddFieldsToSites < ActiveRecord::Migration[5.2]
  def change
    add_column :sites, :user_id, :integer
    add_column :sites, :last_up_at, :datetime
    add_column :sites, :last_down_at, :datetime
    add_column :sites, :response_time, :float
  end
end
