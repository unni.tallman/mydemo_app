class CreateSites < ActiveRecord::Migration[5.2]
  def change
    create_table :sites do |t|
      t.string :name
      t.string :url
      t.boolean :up, default: true

      t.timestamps
    end
  end
end
